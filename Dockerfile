FROM php:7.3-apache
RUN apt update && apt install -y \
        libxml2-dev \
        libzip-dev \
        zlib1g-dev \
        libicu-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev
RUN docker-php-ext-install mysqli pdo_mysql
RUN docker-php-ext-install bcmath
RUN docker-php-ext-configure gd --with-freetype --with-jpeg && docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install intl
RUN docker-php-ext-install soap
RUN docker-php-ext-install zip
RUN docker-php-ext-install sockets
RUN apt update && apt install -y libxslt-dev
RUN docker-php-ext-install xsl
RUN a2enmod rewrite
RUN apt-get install -y cron
