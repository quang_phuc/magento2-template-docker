chmod u+x bin/magento
php -d memory_limit=-1 bin/magento setup:install \
--base-url='http://localhost' \
--db-host='mysql' \
--db-name='ias' \
--db-user='ias' \
--db-password='ias_password' \
--admin-firstname='admin' \
--admin-lastname='admin' \
--admin-email='admin@admin.com' \
--admin-user='admin' \
--admin-password='admin123' \
--language='en_US' \
--currency='USD' \
--timezone='America/Chicago' \
--elasticsearch-host='elastic'
php -d memory_limit=-1 bin/magento cron:install
php -d memory_limit=-1 bin/magento module:disable Magento_TwoFactorAuth
php -d memory_limit=-1 bin/magento setup:di:compile
php -d memory_limit=-1 bin/magento setup:static-content:deploy -f
php -d memory_limit=-1 bin/magento indexer:reindex

